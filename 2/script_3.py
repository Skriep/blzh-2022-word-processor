from pwn import *
import random


random.seed(123456789)
CIPHER_KEY = random.randint(0, 1 << 25)


def encipher(msg: bytes, key: int) -> str:
    res = ''
    for b in msg:
        key = ((key + 0x766f7321) * 0x21766f73) % (1 << 32)
        c = b + key
        res += "0123456789ABCDEF"[(int)(c >> 4) & 0xf]
        res += "0123456789ABCDEF"[(int)(c) & 0xf]
    return res


io = connect('editor.blzh.fr', 11555)

test_text = bytes(list(range(256)))
enc = encipher(test_text, CIPHER_KEY)

io.recvuntil(b'> ')
io.sendline(b'N')
io.recvuntil(b'File name: ')
io.sendline(b'something.dll')

io.recvuntil(b'> ')
io.sendline(b'A')
io.recvuntil(b"Enter text line by line, end with '.' alone on the line")
io.recvline()
io.sendline(enc.encode())
io.sendline(b'.')

io.recvuntil(b'> ')
io.sendline(b'decipher.dll')
io.recvuntil(b'Enter matching secret key: ')
io.sendline(str(CIPHER_KEY).encode())

io.interactive()
