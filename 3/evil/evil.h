#include <io.h>
#include <stdio.h>
#include <windows.h>

#define EXPORT_DLL __declspec(dllexport)


EXPORT_DLL char PLUGIN_DESCR[];

EXPORT_DLL BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved);
EXPORT_DLL char* ProcessInput(char *input, int length, int *outlen);
